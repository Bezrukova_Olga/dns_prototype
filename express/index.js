const express = require('express')
const cors = require('cors');
const path = require("path");
const app = express()
const port = 3000

app.use(cors());

app.use('/static',express.static('images'))
app.get('/json', (req, res) => {
    res.json([
        {
            id: 1,
            category: "smartphone",
            img: "/images/smartphones/1.jpg",
            description: "4\" Смартфон DEXP A440 8 ГБ серый [4x(1.3 ГГц), 1 Гб, 2 SIM, TN, 800x480, камера 2 Мп, 3G, GPS, FM, 1440 мА*ч]",
            price: 2750
        },
        {
            id: 2,
            category: "smartphone",
            img: "../images/smartphones/2.jpg",
            description: "5\" Смартфон DEXP G450 8 ГБ серый [4x(1.3 ГГц), 1 Гб, 2 SIM, TN, 854x480, камера 2 Мп, 3G, GPS, FM, 2000 мА*ч]",
            price: 2899
        },
        {
            id: 3,
            category: "smartphone",
            img: "../images/smartphones/3.jpg",
            description: "5\" Смартфон DEXP A350 16 ГБ синий [4x(1.3 ГГц), 1 Гб, 1 SIM, TN, 960x480, камера 2 Мп, 3G, GPS, FM, 2500 мА*ч]",
            price: 3199
        },
        {
            id: 4,
            category: "smartphone",
            img: "../images/smartphones/4.jpg",
            description: "3.97\" Смартфон BQ 4030G Nice Mini 16 ГБ золотистый [4x(1.3 ГГц), 1 Гб, 2 SIM, TN, 800x480, камера 2 Мп, 3G, GPS, 1550 мА*ч]",
            price: 3299
        },
        {
            id: 5,
            category: "smartphone",
            img: "../images/smartphones/5.jpg",
            description: "3.97\" Смартфон BQ 4030G Nice Mini 16 ГБ черный [4x(1.3 ГГц), 1 Гб, 2 SIM, TN, 800x480, камера 2 Мп, 3G, GPS, 1550 мА*ч]",
            price: 3299
        },
        {
            id: 6,
            category: "photo",
            img: "../images/photo/6.jpg",
            description: "Зеркальная камера Canon EOS 2000D Kit 18-55mm DC черный [24.7 Мп, CMOS, 22.3 х 14.9 мм, 3 кадр./сек, диафрагма - f/3.5-5.6, 100-6400 ISO]",
            price: 39999
        },
        {
            id: 7,
            category: "photo",
            img: "../images/photo/7.jpg",
            description: "Зеркальная камера Canon EOS 2000D Kit 18-55mm IS черный [24.7 Мп, CMOS, 22.3 х 14.9 мм, 3 кадр./сек, диафрагма - f/3.5-5.6, 100-6400 ISO]",
            price: 39999
        },
        {
            id: 8,
            category: "photo",
            img: "../images/photo/8.jpg",
            description: "Зеркальная камера Canon EOS 250D Kit 18-55mm IS STM черный [25.8 Мп, CMOS, 22.3 х 14.9 мм, 5 кадр./сек, диафрагма - f/4.0-5.6, 100-25600 ISO, Wi-Fi, экран - поворотный]",
            price: 54999
        },
        {
            id: 9,
            category: "photo",
            img: "../images/photo/9.jpg",
            description: "Зеркальная камера Pentax K-70 Body черный [24.78 Мп, CMOS, 23.5 х 15.6 мм, 6 кадр./сек, 100-102400 ISO, Wi-Fi, экран - поворотный]",
            price: 59999
        },
        {
            id: 10,
            category: "photo",
            img: "../images/photo/10.jpg",
            description: "Зеркальная камера Canon EOS 850D Kit 18-55mm IS STM черный [25.8 Мп, CMOS, 22.3 х 14.9 мм, 7 кадр./сек, диафрагма - f/4.0-5.6, 100-25600 ISO, экран - поворотный]",
            price: 79999
        }
    ])
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})