import "./MainCenterBlock.css";

const MainCenterBlock = () => {
    return (
        <div className="main-center-block">
            <div className="main-center-block-welcome">Приглашаем<br/>в команду DNS</div>
            <div className="main-center-block-salary">Достойная зарплата</div>
            <div className="main-center-block-promises">Официальное<br/>трудоустройство</div>
            <div className="main-center-block-promises">Бесплатное<br/>обучение</div>
            <div className="main-center-block-promises">Быстрый<br/>карьерный рост</div>
        </div>
    );
}

export default MainCenterBlock;