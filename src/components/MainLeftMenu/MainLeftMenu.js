import Dropdown from "../Dropdown/Dropdown";
import "./MainLeftMenu.css";

const MainLeftMenu = () => {
    return (
        <div className="main-left-menu-block">
            <Dropdown buttonText="Бытовая техника" buttonDescription="для дома уход за собой"
                      content={["Техника для кухни", "Техника для дома", "Красота и здоровье"]}/>
            <Dropdown buttonText="Cмартфоны и гаджеты" buttonDescription="планшеты фототехника"
                      content={["Смартфоны и гаджеты", "Планшеты, электронные книги", "Фототехника"]}/>
            <Dropdown buttonText="ТВ и мультимедиа" buttonDescription="аудио видеоигры"
                      content={["Телевизоры и аксессуары", "Консоли и видеоигры", "Аудиотехника"]}/>
        </div>
    );
}

export default MainLeftMenu;