import "./AllForSchool.css";
import ListItems from "../ListItems/ListItems";
import Card from "../Card/Card";
import lenovo from "../../images/lenovo.jpg";
import lenovo_ideapad from "../../images/lenovo_ideapad.jpg";
import hp from "../../images/HP.jpg";
import acer from "../../images/acer.jpg";

const AllForSchool = (props) => {
    const {description} = props;
    return (
        <div className="all-for-school-block">
            <div className="all-for-school-block-title">Всё для учебы</div>
            <div className="all-for-school-block-offers">
                <div className="all-for-school-block-offers-categories">
                    <div className="categories-theme-active">Ноутбуки</div>
                    <ListItems className="categories-theme"
                               content={["Компьютерные столы", "Персональные компьютеры",
                                   "Кресла и стулья", "Струйные принтеры", "Мониторы", "Планшеты"]}/>
                </div>
                <div className="all-for-school-block-offers-objects">
                    <Card image={lenovo} description={description} price="48 999 ₽"/>
                    <Card image={lenovo_ideapad} description={description} price="32 999 ₽"/>
                    <Card image={hp} description={description} price="93 999 ₽"/>
                    <Card image={acer} description={description} price="57 999 ₽"/>
                </div>
            </div>
        </div>
    );
};

export default AllForSchool;