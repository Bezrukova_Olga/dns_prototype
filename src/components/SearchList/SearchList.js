import "./SearchList.css";

const SearchList = (props) => {
    const {searchList, setSearchList, saveDataFromSearch} = props;

    const remove = (value) => {
        const newSearchList = searchList.filter((item) => item !== value);
        setSearchList(newSearchList);
        saveDataFromSearch(newSearchList);
    };

    return searchList.length ? (
        <div id="search-dropdown-list" className="search-list">
            {searchList.map((value, index) => (
                <div key={index} className="search-list-element">
                    {value}
                    <button
                        onClick={(e) => {
                            if (e.target.innerText === "X") {
                                remove(value);
                                value = "";
                            }
                        }}
                    >
                        X
                    </button>
                </div>
            ))}
        </div>
    ) : (
        <></>
    );
};

export default SearchList;
