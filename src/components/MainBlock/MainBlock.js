import "./MainBlock.css";
import AllForSchool from "../AllForSchool/AllForSchool";
import MainLeftMenu from "../MainLeftMenu/MainLeftMenu";
import MainCenterBlock from "../MainCenterBlock/MainCenterBlock";
import MainRightBlock from "../MainRightBlock/MainRightBlock";
import MainActualOffers from "../MainActualOffers/MainActualOffers";
import MainBottomBlock from "../MainBottomBlock/MainBottomBlock";

const MainBlock = () => {
    let description = (
        <div>15.6" Ноутбук Lenovo V15 ADA серый<br/>[Full HD (1920x1080),TN+film,AMD<br/>Ryzen 3 3250U,ядра: 2 х 2.6
            ГГц,RAM<br/>8 ГБ,SSD 256 ГБ,AMD Radeon<br/>Graphics,Windows 10 Pro]</div>
    )

    return (
        <div className="main">
            <div className="main-left-menu">
                <MainLeftMenu/>
                <div className="main-right-menu-block">
                    <MainCenterBlock/>
                    <MainRightBlock/>
                    <MainActualOffers/>
                    <MainBottomBlock/>
                </div>
            </div>
            <AllForSchool description={description}/>
        </div>
    );
}

export default MainBlock;