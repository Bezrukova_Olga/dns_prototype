import "./Header.css";
import Search from "../Search/Search";
import ListItems from "../ListItems/ListItems";
import {useHistory} from "react-router-dom";

const Header = () => {
    const history = useHistory();

    function handleClick(path) {
        history.push(path);
    }

    return (
        <div>
            <div className="header">
                <div className="banner">
                    <h1 className="banner-logo">Школьная распродажа</h1>
                </div>
                <div className="advertising">
                    <span className="advertising-left">Воронеж</span>
                    <div className="advertising-center">
                        <span className="advertising-center-elements">Магазины</span>
                        <span className="advertising-center-elements">Покупателям</span>
                        <span className="advertising-center-elements">Юридическим лицам</span>
                        <span className="advertising-center-elements">Клуб DNS</span>
                    </div>
                    <span className="advertising-right">8-800-77-07-999 (c 03:00 до 22:00)</span>
                </div>
            </div>
            <div className="search_line_elements">
                <div className="search">
                    <div className="dns_logo" onClick={() => handleClick('/')}>DNS</div>
                    <Search/>
                    <ListItems className="search_style"
                               content={["Сравнить", "Избранное", "Корзина", "Войти"]}/>
                </div>
            </div>
        </div>
    );
}

export default Header;