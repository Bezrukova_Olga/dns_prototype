import "./Dropdown.css";
import {useHistory} from "react-router-dom";

const Dropdown = (props) => {
    const {buttonText, buttonDescription, content} = props;
    const history = useHistory();

    function handleClick(path) {
        history.push(path);
    }

    const listItems = content.map((element, index) =>
        <div>
            {element === 'Фототехника' ? (<a key={index} onClick={() => handleClick('/photos')}>{element}</a>) : (
                element === 'Смартфоны и гаджеты' ? (
                    <a key={index} onClick={() => handleClick('/smartphones')}>{element}</a>) : (
                    <a key={index} href="#">{element}</a>)
            )}
        </div>
    );
    return (
        <div className="dropdown">
            <button className="dropbtn">{buttonText}<br/>
                <div className="dropbtn-br-style">{buttonDescription}</div>
            </button>
            <div className="dropdown-content">
                {listItems}
            </div>
        </div>
    );
};

export default Dropdown;