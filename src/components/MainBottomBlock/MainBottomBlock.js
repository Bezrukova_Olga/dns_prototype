import ListItems from "../ListItems/ListItems";
import "./MainBottomBlock.css";

const MainBottomBlock = () => {
    //TODO: make it universal
    return (
        <>
            <div className="main-bottom-blocks">
                <div className="main-bottom-blocks-title">Привет!</div>
                <div className="main-bottom-blocks-point">Получай бонусы и спецпредложения,<br/>сохраняй и
                    отслеживай заказы
                </div>
                <ListItems className="main-bottom-blocks-buttons"
                           content={["Перейти в профиль", "Мои заказы"]}/>
            </div>
            <div className="main-bottom-blocks">
                <div className="main-bottom-blocks-title">Собери свой ПК</div>
                <div className="main-bottom-blocks-point">Простой инструмент без проблем<br/>с
                    совместимостью
                </div>
                <ListItems className="main-bottom-blocks-buttons"
                           content={["Собрать ПК", "Сборки пользователей"]}/>
            </div>
        </>
    );
}

export default MainBottomBlock;