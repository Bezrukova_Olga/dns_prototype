import "./Footer.css";
import { useHistory } from "react-router-dom";

const Footer = () => {
    const history = useHistory();
    function handleClick() {
        history.push("/about");
    }
    return (
        <div>
            <div className="footer-main-style">
                <div className="footer-style">
                    <div className="footer-about-company" onClick={() => handleClick()}>О компании</div>
                </div>
            </div>
        </div>
    );
}

export default Footer;