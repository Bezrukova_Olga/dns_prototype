import "./MainActualOffers.css";
import ListItems from "../ListItems/ListItems";

const MainActualOffers = () => {
    return (
        <>
            <div className="main-center-page-block-title">Актуальные предложения</div>
            <div className="main-current-offers">
                <div className="main-current-offers-button">
                    <button className="main-current-offers-button-theme-active">Все для учебы</button>
                </div>
                <ListItems className="main-current-offers-button" content={["Зима близко",
                    "Будь на связи", "Осенние заготовки", "Чистая вода"]}
                           buttonClassName="main-current-offers-button-theme"/>
            </div>
        </>
    );
}

export default MainActualOffers;