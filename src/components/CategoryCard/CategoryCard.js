import "./CategoryCard.css";
import React from "react";
import {
    add,
    deleteAll,
    deleteById,
} from "../../store/productStore";
import {useDispatch} from "react-redux";

const CategoryCard = (props) => {
    const {product, isBasket} = props;
    const dispatch = useDispatch();
    return (
        <div>
            <div key={product.id} className="category-card-main-style">
                <div><img className="category-card_image" src={product.img} alt=""/></div>
                <div className="category-card_description">{product.description}</div>
                <div className="category-card_price-common">
                    {isBasket ? (
                        <div className="category-card_price">{product.price * product.count}₽</div>) : (
                        <div className="category-card_price">{product.price}₽</div>)}
                    <div>
                        {
                            isBasket ? (
                                <>
                                    <button className="category-card-button category-card-button-theme-text"
                                            onClick={() => {
                                                dispatch(deleteAll(product));
                                            }}
                                    >
                                        Удалить
                                    </button>
                                    <button className="category-card-button category-card-button-theme"
                                            onClick={() => {
                                                dispatch(deleteById(product));
                                            }}
                                    >
                                        -
                                    </button>
                                    <div className="category-card_basket-container">
                                        <div className="category-card_basket-text">{product.count ?? 0}</div>
                                    </div>
                                    <button className="category-card-button category-card-button-theme"
                                            onClick={() => {
                                                dispatch(add(product));
                                            }}
                                    >
                                        +
                                    </button>


                                </>
                            ) : (
                                <>
                                    <button className="category-card-button category-card-button-theme-text"
                                            onClick={() => {
                                                dispatch(add(product));
                                            }}
                                    >Купить
                                    </button>
                                    <button className="category-card-button category-card-button-theme">♡</button>
                                </>
                            )
                        }
                    </div>
                </div>
            </div>
        </div>
    );
};

export default CategoryCard;