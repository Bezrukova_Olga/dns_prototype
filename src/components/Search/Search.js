import "./Search.css";
import SearchList from "../SearchList/SearchList";
import {useCallback, useEffect, useState} from "react";

const Search = ({children, onBlur, ...props}) => {
    const [searchList, setSearchList] = useState([]);
    const [isVisible, setVisible] = useState(false);

    useEffect(() => {
        setSearchList(loadDataFromSearch());
    }, []);

    const saveDataFromSearch = (data) => {
        localStorage.removeItem("search");
        localStorage.setItem("search", JSON.stringify(data));
        setSearchList(data);
    };

    const loadDataFromSearch = () => {
        const items = localStorage.getItem("search");
        return items ? JSON.parse(items) : [];
    };

    const handleBlur = useCallback(
        (e) => {
            const currentTarget = e.currentTarget;

            // Give browser time to focus the next element
            requestAnimationFrame(() => {
                // Check if the new focused element is a child of the original container
                if (!currentTarget.contains(document.activeElement)) {
                    setVisible(false)
                }
            });
        },
        [onBlur]
    );

    return (
        <>
            <div className="input_settings" onBlur={handleBlur}>
                <input id="search-input" className="search-input" type="text" autoComplete="false"
                       onFocus={() => {
                           setVisible(true);
                       }}
                       placeholder="Поиск по сайту"/>
                {isVisible ? (
                    <SearchList
                        searchList={searchList}
                        setSearchList={setSearchList}
                        saveDataFromSearch={saveDataFromSearch}
                    />
                ) : (
                    <></>
                )}
            </div>
            <button className="search_button" id="search-but" onClick={() => {
                const search = document.getElementById("search-input");
                const values = loadDataFromSearch();
                if (!values.includes(search.value) && search.value !== "") {
                    values.push(search.value);
                    saveDataFromSearch(values);
                }
                search.value = "";
            }}>Поиск
            </button>

        </>
    );
}

export default Search;