import ListItems from "../ListItems/ListItems";
import "./MainRightBlock.css";

const MainRightBlock = () => {
    return (
        <div className="main-right-block">
            <div className="main-right-block-title">Акции</div>
            <ListItems className="main-right-block-point" content={["Скидки и предложения 124",
                "Рассрочка и выгода 22", "Выгодные комплекты 66"]}/>
            <ListItems className="main-right-block-button" content={["Все акции"]}
                       buttonClassName="orange-button-theme"/>
            <ListItems className="main-right-block-button" content={["Все товары"]}
                       buttonClassName="black-button-theme"/>
        </div>
    );
}

export default MainRightBlock;