import "./Content.css";
import MainBlock from "../MainBlock/MainBlock"

const Content = () => {
    return (
        <div>
            <MainBlock/>
        </div>
    );
}

export default Content;