import "./ListItems.css";
import { useHistory } from "react-router-dom";

const ListItems = (props) => {
    const {className, content, buttonClassName} = props;
    const history = useHistory();
    function handleClick() {
        history.push("/basket");
    }
    return !buttonClassName ? (
        content.map((element, index) =>
            <div>
                {(element === 'Корзина') ? (
                    <div key={index} onClick={() => handleClick()} className={className}>{element}</div>) : (
                    <div key={index} className={className}>{element}</div>)}
            </div>
        )) : (
        content.map((element, index) =>
            <div key={index} className={className}>
                <button className={buttonClassName}>{element}</button>
            </div>
        ));
};



export default ListItems;