import "./Card.css";
import ListItems from "../ListItems/ListItems";

const Card = (props) => {
    const {image, description, price} = props;
    //TODO: fix problem <div> cannot appear as a descendant of <p>
    return (
        <div className="all-for-school-block-offers-objects-element">
            <p><img className="picture" src={image} alt=""/></p>
            <p className="all-for-school-test-size">{description}</p>
            <ListItems className="all-for-school-block-offers-objects-button"
                       content={[price]} buttonClassName="all-for-school-button-price-theme"/>
            <ListItems className="all-for-school-block-offers-objects-button"
                       content={["♡", "🛒"]} buttonClassName="all-for-school-button-theme"/>
        </div>
    );
};

export default Card;