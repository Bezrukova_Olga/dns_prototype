import "./Products.css";
import CategoryCard from "../../components/CategoryCard/CategoryCard";
import React from "react";

const Products = (props) => {
    const {products, isBasket} = props;
    return (
        <div>
            <div key={Math.random()} className="products-main-style">
                <div key={Math.random()} className="products-inner-style">
                    {products.map((product) => (
                        <CategoryCard key={Math.random()} isBasket={isBasket} product={product} />
                    ))}
                </div>
            </div>

        </div>
    )
}
export default Products;

