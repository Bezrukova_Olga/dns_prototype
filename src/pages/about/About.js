import "./About.css";

const About = () => {
    return (
        <div className="about-main-style">
            <div className="about-inner-style">
                <h1 className="company-about_title">О компании</h1>
                <div className="company-about_container">
                    <div className="company-about_navigation">
                        <div className="menu">
                            <div className="menu_list">
                                <div className="menu_item">
                                    <div>О компании</div>
                                </div>
                                <div className="menu_item">
                                    <div>Карьера</div>
                                </div>
                                <div className="menu_item">
                                    <div>Сотрудничество</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="company-about_content">
                        <div className="page-about">
                            <div className="about-dns">
                                <div className="about-dns_title">
                                    <h1>DNS – один из лидеров рынка по продаже цифровой и бытовой техники в России</h1>
                                </div>
                                <div className="about-dns_subtitle">
                                    <div className="about-dns_top-subtitle">Наша цель изменить жизнь людей, сделав
                                        простым доступ к огромному количеству качественных и недорогих товаров,
                                        предоставляя лучший сервис.
                                    </div>
                                </div>
                                <div className="about-dns_title">
                                    <h1>Наша история</h1>
                                </div>
                                <div className="about-dns_elements">
                                    <div className="about-dns_element">
                                        <div className="about-dns_element-title">1998 г.</div>
                                        <div className="about-dns_element-text">Открытие 1 магазина в г. Владивосток</div>
                                    </div>
                                    <div className="about-dns_element">
                                        <div className="about-dns_element-title">2006 г.</div>
                                        <div className="about-dns_element-text">Запуск собственного производства</div>
                                    </div>
                                    <div className="about-dns_element">
                                        <div className="about-dns_element-title">2007 г.</div>
                                        <div className="about-dns_element-text">Выход на рынок Дальнего востока и Восточной Сибири</div>
                                    </div>
                                    <div className="about-dns_element">
                                        <div className="about-dns_element-title">2009 г.</div>
                                        <div className="about-dns_element-text">Экспансия на рынок РФ</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default About;
