import {configureStore} from "@reduxjs/toolkit";
import productStoreReducer from "./productStore";

export const store = configureStore({
    reducer: {
        products: productStoreReducer,
    },
});
