import {createSlice} from "@reduxjs/toolkit";

const initialState = {products: [], basket: []};

export const productStore = createSlice({
    name: "product",
    initialState,
    reducers: {
        add: (state, action) => {
            if (!state.basket.map((element) => element.id).includes(action.payload.id)) {
                state.basket.push({ ...action.payload, count: 1 });
            } else {
                const existElement = state.basket.find((element) => element.id === action.payload.id);
                existElement.count += 1;
            }
            localStorage.removeItem("basket");
            localStorage.setItem("basket", JSON.stringify(state.basket));
        },
        deleteById: (state, action) => {
            const element = state.basket.find(
                (element) => element.id === action.payload.id
            );
            if (element.count !== 1) {
                element.count -= 1;
            } else {
                state.basket = state.basket.filter(
                    (element) => element.id !== action.payload.id
                );
            }
            localStorage.removeItem("basket");
            localStorage.setItem("basket", JSON.stringify(state.basket));
        },
        deleteAll: (state, action) => {
            state.basket = state.basket.filter(
                (element) => element.id !== action.payload.id
            );
            localStorage.removeItem("basket");
            localStorage.setItem("basket", JSON.stringify(state.basket));
        },
        importProducts: (state, action) => {
            state.products = action.payload;
            console.log(state.products)
        },
        getBasket: (state) => {
            state.basket = JSON.parse(localStorage.getItem("basket")) ?? [];
        },
    },
});

export const {
    importProducts,
    add,
    deleteAll,
    getBasket,
    deleteById,
} = productStore.actions;

export default productStore.reducer;