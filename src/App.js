import './App.css';
import About from "./pages/about/About"
import Home from "./pages/home/Home"
import {
    BrowserRouter,
    Route,
    Switch
} from "react-router-dom";
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import Products from "./pages/products/Products";
import {SMARTPHONE, PHOTO} from "./resources/categories";
import {useDispatch, useSelector} from "react-redux";
import {useEffect} from "react";
import {
    getBasket,
    importProducts,
} from "./store/productStore";


function App() {
    const dispatch = useDispatch();

    useEffect(() => {
        fetch('http://localhost:3000/json')
            .then((res) => res.json())
            .then((data) => {
                console.log(data);
                dispatch(importProducts(data));
            })
            .catch((err) => {
                console.log(err.message);
            });
        dispatch(getBasket());
    }, [dispatch]);

    const products = useSelector((state) => state.products.products);
    const basket = useSelector((state) => state.products.basket);
    return (
        <BrowserRouter>
            <Header/>
            <Switch>
                <Route exact path='/'>
                    <Home/>
                </Route>
                <Route path='/smartphones'>
                    <Products products={products.filter((product) => product.category === SMARTPHONE)}/>
                </Route>
                <Route path='/photos'>
                    <Products products={products.filter((product) => product.category === PHOTO)}/>
                </Route>
                <Route path='/about'>
                    <About/>
                </Route>
                <Route path="/basket">
                    <Products products={basket} isBasket/>
                </Route>
            </Switch>
            <Footer/>
        </BrowserRouter>
    );
}

export default App;
